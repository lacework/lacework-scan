#!/usr/bin/env bats

setup() {
  echo "Building pipe image..."
  echo "DOCKER_HOST: $DOCKER_HOST"
  echo "BITBUCKET_DOCKER_HOST_INTERNAL: $BITBUCKET_DOCKER_HOST_INTERNAL"

  PIPE_IMAGE=${PIPE_IMAGE:="lacework/lacework-scan"}
  run docker build -t ${PIPE_IMAGE} .

  echo "Pulling test image..."
  docker pull jefferyfry/vuln-app:latest
  docker images
}

run_pipe() {
  FIXTURE_DIR="$(pwd)/$1"

  echo "DOCKER_HOST: $DOCKER_HOST"
  echo "BITBUCKET_DOCKER_HOST_INTERNAL: $BITBUCKET_DOCKER_HOST_INTERNAL"
  echo "Extra params $@"
  echo "Run image ${PIPE_IMAGE}"
  run docker container run \
    --volume=/usr/local/bin/docker:/usr/local/bin/docker:ro \
    --volume=/var/run/docker.sock:/var/run/docker.sock \
    --env=DOCKER_HOST="tcp://$BITBUCKET_DOCKER_HOST_INTERNAL:2375" \
    --add-host="host.docker.internal:$BITBUCKET_DOCKER_HOST_INTERNAL" \
    --env=BITBUCKET_DOCKER_HOST_INTERNAL="$BITBUCKET_DOCKER_HOST_INTERNAL" \
    --workdir=$FIXTURE_DIR \
    --volume=$FIXTURE_DIR:$FIXTURE_DIR \
    --env=BITBUCKET_CLONE_DIR="$FIXTURE_DIR" \
    --env=BITBUCKET_REPO_OWNER="lacework" \
    --env=BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
    --env=BITBUCKET_COMMIT="$BITBUCKET_COMMIT" \
    --env=BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
    --env=LW_ACCOUNT_NAME=${LW_ACCOUNT_NAME} \
    --env=LW_ACCESS_TOKEN=${LW_ACCESS_TOKEN} \
    $@ \
    ${PIPE_IMAGE}
}

@test "Fail on vulnerabilities" {
    run_pipe \
      --env=IMAGE_NAME=jefferyfry/vuln-app \
      --env=IMAGE_TAG=latest \

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}

