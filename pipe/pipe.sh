#!/usr/bin/env bash
#
# This pipe is an example to show how easy is to create pipes for Bitbucket Pipelines.
#

source "$(dirname "$0")/common.sh"

telemetry_post_data()
{
  cat <<EOF
{
  "account": "$LW_ACCOUNT_NAME",
  "sub-account": "",
  "tech-partner": "Atlassian",
  "integration-name": "lacework-scan-bitbucket-pipe",
  "version": "1.1.0",
  "service": "Bitbucket Pipelines",
  "install-method": "",
  "function": "pipe.sh",
  "event": "scan",
  "event-data": ""
}
EOF
}

curl -i \
-H "Accept: application/json" \
-H "Content-Type:application/json" \
-H "X-Honeycomb-Team:d8f23a18deba09087c5c171f555893c0" \
-X POST --data "$(telemetry_post_data)" "https://api.honeycomb.io/1/events/lacework-alliances-prod" || true

info "Executing the lw-scanner to discover vulnerabilities..."

# Required parameters
LW_ACCOUNT_NAME=${LW_ACCOUNT_NAME:?'LW_ACCOUNT_NAME variable missing.'}
LW_ACCESS_TOKEN=${LW_ACCESS_TOKEN:?'LW_ACCESS_TOKEN variable missing.'}
IMAGE_NAME=${IMAGE_NAME:?'IMAGE_NAME variable missing.'}
IMAGE_TAG=${IMAGE_TAG:?'IMAGE_TAG variable missing.'}

# Default parameters
SAVE_RESULTS_IN_LACEWORK=${SAVE_RESULTS_IN_LACEWORK:="true"}
SAVE_BUILD_REPORT=${SAVE_BUILD_REPORT:="true"}

export LW_ACCOUNT_NAME=${LW_ACCOUNT_NAME}
export LW_ACCESS_TOKEN=${LW_ACCESS_TOKEN}
export LW_SCANNER_DISABLE_UPDATES=true

if [ ${SAVE_RESULTS_IN_LACEWORK} == "true" ]; then
  export SCANNER_PARAMETERS+=" --save"
fi

if [ ${SAVE_BUILD_REPORT} == "true" ]; then
  export SCANNER_PARAMETERS+=" --html"
fi

if [ -z ${BITBUCKET_REPO_SLUG} ]; then
    echo "BITBUCKET_REPO_SLUG isn't set."
  else
    export SCANNER_PARAMETERS+=" --build-plan ${BITBUCKET_REPO_SLUG}"
fi

if [ -z ${BITBUCKET_BUILD_NUMBER} ]; then
  BITBUCKET_BUILD_NUMBER=$(date +%s)
fi

if [ -z ${BITBUCKET_WORKSPACE} ]; then
  BITBUCKET_WORKSPACE="./tmp"
fi

/opt/lacework/lw-scanner image evaluate ${IMAGE_NAME} ${IMAGE_TAG} --build-id ${BITBUCKET_BUILD_NUMBER} --data-directory ${BITBUCKET_WORKSPACE} --policy --fail-on-violation-exit-code 1 ${SCANNER_PARAMETERS}


