# Bitbucket Pipelines Pipe: lacework-scan

![Lacework](https://www.lacework.com/wp-content/themes/lacework/build/images/company-logo.png)

The Lacework inline scanner allows you to integrate Lacework security capabilities deep into your CI/CD by allowing you to scan and assess Docker container images and software packages for vulnerabilities. This Lacework Scan Bitbucket Pipe wraps the inline scanner and allows you easily scan your Docker images in your Bitbucket Pipelines.

Example code with a demo application and a Bitbucket Pipeline using the Lacework Scan pipe is [here](https://bitbucket.org/lacework-alliances/shift-left-devsecops-atlassian-lacework).

Container vulnerability policies must be set in Lacework in order to block CI/CD builds. Learn how to create a policy [here](https://docs.lacework.com/container-vulnerability-policies) and attach the policy to the inline scanner integration.
![policy](https://bitbucket.org/lacework/lacework-scan/raw/ea96f141920197f1d37217503fa37cb6215ab80b/img/policy.png)
Learn more about the Lacework lw-scanner [here](https://support.lacework.com/hc/en-us/articles/1500001777821-Integrate-Inline-Scanner).

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: lacework/lacework-scan:1.2.0
    variables:
      LW_ACCOUNT_NAME: "<string>" # Required
      LW_ACCESS_TOKEN: "<string>" # Required
      IMAGE_NAME: "<string>" # Required
      IMAGE_TAG: "<string>" # Required
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| LW_ACCOUNT_NAME (*)   | Your Lacework account name. For example, if your login URL is mycompany.lacework.net, the account name is mycompany. |
| LW_ACCESS_TOKEN (*)   | Authorization token. Copy and paste the token from the inline scanner integration created in the Lacework Console. |
| IMAGE_NAME (*)        | The local Docker image to scan |
| IMAGE_TAG (*)         | The local Docker image tag |

_(*) = required variable._

## Prerequisites
You must have a Lacework account to use this pipe.

## Examples

Basic example:

```yaml
script:
  - docker build . -t "${IMAGE_NAME}:${BITBUCKET_BUILD_NUMBER}"
  - pipe: lacework/lacework-scan:1.2.0
    variables:
      LW_ACCOUNT_NAME: foobar
      LW_ACCESS_TOKEN: xdfefafda
      IMAGE_NAME: ${IMAGE_NAME}
      IMAGE_TAG: ${BITBUCKET_BUILD_NUMBER}
```

## Support
If youâ€™d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by jeff.fry@lacework.net.

If you're reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
