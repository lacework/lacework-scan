# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.2.0

- minor: Updated lacework-inline-scanner base image.

## 1.1.6

- patch: Fixed telemetry.

## 1.1.5

- patch: Fixed telemetry.

## 1.1.4

- patch: Fixed telemetry.
- patch: Fixed telemetry.
- patch: Fixed telemetry.
- patch: Fixed telemetry.

## 1.1.3

- patch: Fixed telemetry.

## 1.1.2

- patch: Fixed telemetry.

## 1.1.1

- patch: Added telemetry.

## 1.1.0

- minor: Updated tests. Fixed violation handling.
- minor: Updated to use the new lacework-inline-scanner.
- patch: Debug.
- patch: Fixed violations flag.
- patch: Fixed violations flag.
- patch: Fixed violations flag.
- patch: Fixed violations flag.
- patch: Fixed violations flag.
- patch: Fixed violations flag.
- patch: Fixed violations flag.
- patch: Fixed violations flag.
- patch: Fixed violations flag.
- patch: Updated README.
- patch: Updated tests.
- patch: Updated tests.
- patch: Updated tests.
- patch: Updated tests.
- patch: Updated tests.
- patch: Updated tests.
- patch: Updated tests.
- patch: Updated tests.
- patch: Updated tests.
- patch: Updated tests.
- patch: Updated tests.
- patch: Updated tests.

## 1.0.0

- major: Updated example YAML to reference this specific release. Ready to release pipe.

## 0.1.13

- patch: Updated bitbucket-pipe-release and changed lacework-scan repo.

## 0.1.12

- patch: Changed repo to lacework.

## 0.1.11

- patch: Changed repo and pipe name to lacework-scan.

## 0.1.10

- patch: Updated README and added test cases.

## 0.1.9

- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.
- patch: Updated test cases.

## 0.1.8

- patch: Updated test cases.

## 0.1.7

- patch: Fixed test cases.
- patch: Fixed test cases.
- patch: Fixed test cases.
- patch: Fixed type in pipe shell.
- patch: Pipe shell updates.
- patch: Updated pipe shell.
- patch: Updated pipe shell.
- patch: Updated pipe shell.
- patch: Updated pipe shell.
- patch: Updated pipe shell.
- patch: Updated pipe shell.
- patch: Updated pipe shell.
- patch: Updated pipe shell.
- patch: Updated pipe shell.
- patch: Updated pipe shell.
- patch: Updated pipe shell.
- patch: Updated pipe shell.
- patch: Updated pipe shell.
- patch: Updated pipe shell.

## 0.1.6

- patch: Added pipeline debug.
- patch: Removed hung test case.

## 0.1.5

- patch: Added options to increase docker limits.
- patch: Changed test image.
- patch: Removed fail test due to bitbucket hanging.
- patch: Updated pipe shell logic. Add test case.

## 0.1.4

- patch: Added jq to Dockerfile. Updated README.

## 0.1.3

- patch: Updated pipe to be comparable to github actions.

## 0.1.2

- patch: Changed dockerhub repo.
- patch: Updated dockerfile and base image.

## 0.1.1

- patch: Initial beta release.

## 0.1.0

- minor: Initial release
