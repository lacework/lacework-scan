FROM lacework/lacework-inline-scanner:latest

ARG HONEY_KEY
ARG HONEY_DATASET

RUN apk add --update --no-cache bash
RUN apk --no-cache add curl
RUN apk --no-cache add jq

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /
RUN curl https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.4.0/common.sh --output common.sh

RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
